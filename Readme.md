### Contains builds frozen in time

> This repository is part (submodule) of https://gitlab.fit.cvut.cz/pavlafil/sharpslam. The superior repository contains all files related with this project. This repository is reserved only for code versioning.


TODO: Automatic builds
 - now not possible at least for .Net461 application because it contains WPF project and fitlab.fit.cvut.cz has only linux runners. Project must be compiled on linux or new runners need to be added